//
//  model.swift
//  assign3
//  Omar Amin
//  4/07/2021
//

import Foundation

enum Direction {
    case left, right, down, up
}

struct ScoreType: Hashable {
    var score: Int
    var time: Date
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(time)
    }
    
    init(score: Int, time: Date) {
        self.score = score
        self.time = time
    }
}


class Triples: ObservableObject {
    @Published var board: [[Int]] = [[Int]](repeating: [Int](repeating: 0, count: 4), count: 4)
    @Published var score: Int = 0
    @Published var isDone: Bool = false
    @Published var allScores: [ScoreType] = []
    var seededGenerator = SeededGenerator(seed: 14)
    var avail: [Bool] = [Bool](repeating: true, count: 16)
    
    func newgame(rand: Bool) {
        board = [[Int]](repeating: [Int](repeating: 0, count: 4), count: 4)
        avail = [Bool](repeating: true, count: 16)
        score = 0
        isDone = false
        if rand == false {
            seededGenerator = SeededGenerator(seed: UInt64(Int.random(in:1...1000)))
        }
        else {
            seededGenerator = SeededGenerator(seed: 14)
        }
    }
    
    func spawn() {
        var spawn: Int = 0
        var placement: Int = 0
        
        spawn = Int.random(in: 1...2, using: &seededGenerator)
        
        let test = avail.filter { $0 == false }.count
        if test != 16 {
            let spaces: Int = avail.filter { $0 == true }.count
            placement = Int.random(in: 1...spaces, using: &seededGenerator)
            
            var count: Int = 0
            for i in 0...15 {
                if avail[i] == true {
                    count += 1
                    if count == placement {
                        placement = i + 1
                        avail[i] = false
                        break
                    }
                }
            }
            score += spawn
                
            switch placement {
            case 1:
                board[0][0] = spawn
            case 2:
                board[0][1] = spawn
            case 3:
                board[0][2] = spawn
            case 4:
                board[0][3] = spawn
            case 5:
                board[1][0] = spawn
            case 6:
                board[1][1] = spawn
            case 7:
                board[1][2] = spawn
            case 8:
                board[1][3] = spawn
            case 9:
                board[2][0] = spawn
            case 10:
                board[2][1] = spawn
            case 11:
                board[2][2] = spawn
            case 12:
                board[2][3] = spawn
            case 13:
                board[3][0] = spawn
            case 14:
                board[3][1] = spawn
            case 15:
                board[3][2] = spawn
            case 16:
                board[3][3] = spawn
            default:
                _ = board[0][0]
            }
        }
    }

    func rotate() {
        board = rotate2DInts(input: board)
    }
    
    func shift() {
        for i in 0..<board.count {
            var flag:Bool = false
            for j in 0..<board.count {
                let left = board[i][0]
                let right = board[i][1]
                if j == 0 {
                    if left == right && left % 3 == 0 && right % 3 == 0 || left == 2 && right == 1 || left == 1 && right == 2 || left == 0 {
                        board[i][0] += board[i][1]
                        if left != 0 {
                            score += board[i][0]
                        }
                        flag = true
                    }
                }
                else if j == 1 || j == 2 {
                    if flag == true {
                        board[i][j] = board[i][j+1]
                    }
                    else {
                        let l = board[i][j]
                        let r = board[i][j+1]
                        if l == r && l % 3 == 0 && r % 3 == 0 || l == 2 && r == 1 || l == 1 && r == 2 || l == 0 {
                            board[i][j] += board[i][j+1]
                            if l != 0 {
                                score += board[i][j]
                            }
                            flag = true
                        }
                    }
                }
                else {
                    if flag == true {
                        board[i][3] = 0
                    }
                }
            }
        }
    }
    
    func collapse(dir: Direction) -> Bool {
        let start: [[Int]] = self.board
        switch dir {
        case .left:
            shift()
        case .right:
            rotate()
            rotate()
            shift()
            rotate()
            rotate()
        case .down:
            rotate()
            shift()
            rotate()
            rotate()
            rotate()
        case .up:
            rotate()
            rotate()
            rotate()
            shift()
            rotate()
        }
        if start == self.board {
            clean()
            return false
        }
        else {
            clean()
            return true
        }
    }
    
    func gameOver() {
        let copy: Triples = Triples()
        
        for i in 0..<board.count {
            for j in 0..<board.count {
                copy.board[i][j] = self.board[i][j]
            }
        }
        
        if copy.collapse(dir: .up) == false && copy.collapse(dir: .left) == false && copy.collapse(dir: .right) == false && copy.collapse(dir: .down) == false {
            self.allScores.append(ScoreType(score: self.score, time: Date()))
            self.isDone = true
        }
    }
    
    func clean() {
        for i in 0..<board.count {
            for j in 0..<board.count {
                if i == 0 && j == 0 {
                    if board[i][j] == 0 {
                        avail[0] = true
                    }
                    else if avail[0] == true {
                        avail[0] = false
                    }
                }
                if i == 0 && j == 1 {
                    if board[i][j] == 0 {
                        avail[1] = true
                    }
                    else if avail[1] == true {
                        avail[1] = false
                    }
                }
                if i == 0 && j == 2 {
                    if board[i][j] == 0 {
                        avail[2] = true
                    }
                    else if avail[2] == true {
                        avail[2] = false
                    }
                }
                if i == 0 && j == 3 {
                    if board[i][j] == 0 {
                        avail[3] = true
                    }
                    else if avail[3] == true {
                        avail[3] = false
                    }
                }
                if i == 1 && j == 0 {
                    if board[i][j] == 0 {
                        avail[4] = true
                    }
                    else if avail[4] == true {
                        avail[4] = false
                    }
                }
                if i == 1 && j == 1 {
                    if board[i][j] == 0 {
                        avail[5] = true
                    }
                    else if avail[5] == true {
                        avail[5] = false
                    }
                }
                if i == 1 && j == 2 {
                    if board[i][j] == 0 {
                        avail[6] = true
                    }
                    else if avail[6] == true {
                        avail[6] = false
                    }
                }
                if i == 1 && j == 3 {
                    if board[i][j] == 0 {
                        avail[7] = true
                    }
                    else if avail[7] == true {
                        avail[7] = false
                    }
                }
                if i == 2 && j == 0 {
                    if board[i][j] == 0 {
                        avail[8] = true
                    }
                    else if avail[8] == true {
                        avail[8] = false
                    }
                }
                if i == 2 && j == 1 {
                    if board[i][j] == 0 {
                        avail[9] = true
                    }
                    else if avail[9] == true {
                        avail[9] = false
                    }
                }
                if i == 2 && j == 2 {
                    if board[i][j] == 0 {
                        avail[10] = true
                    }
                    else if avail[10] == true {
                        avail[10] = false
                    }
                }
                if i == 2 && j == 3 {
                    if board[i][j] == 0 {
                        avail[11] = true
                    }
                    else if avail[11] == true {
                        avail[11] = false
                    }
                }
                if i == 3 && j == 0 {
                    if board[i][j] == 0 {
                        avail[12] = true
                    }
                    else if avail[12] == true {
                        avail[12] = false
                    }
                }
                if i == 3 && j == 1 {
                    if board[i][j] == 0 {
                        avail[13] = true
                    }
                    else if avail[13] == true {
                        avail[13] = false
                    }
                }
                if i == 3 && j == 2 {
                    if board[i][j] == 0 {
                        avail[14] = true
                    }
                    else if avail[14] == true {
                        avail[14] = false
                    }
                }
                if i == 3 && j == 3 {
                    if board[i][j] == 0 {
                        avail[15] = true
                    }
                    else if avail[15] == true {
                        avail[15] = false
                    }
                }
            }
        }
    }
    
    init() {
        newgame(rand: false)
        for _ in 0...3 {
            spawn()
        }
        allScores.append(ScoreType(score: 400, time: Date()))
        allScores.append(ScoreType(score: 300, time: Date()))
    }
    
    init(new: [[Int]]) {
        board = new
    }
}

public func rotate2DInts(input: [[Int]]) -> [[Int]] {
    var new: [[Int]] = [[Int]](repeating: [Int](repeating: 0, count: input.count), count: input.count)
    
    for i in 0..<input.count {
        for j in 0..<input.count {
            new[i][j] = input[j][i]
        }
        new[i].reverse()
    }
    return new
}

public func rotate2D<T>(input: [[T]]) -> [[T]] {
    var new: [[T]] = [[T]](repeating: [T](repeating: input[0][0], count: input.count), count: input.count)
    
    for i in 0..<input.count {
        for j in 0..<input.count {
            new[i][j] = input[j][i]
        }
        new[i].reverse()
    }
    return new
}
