//
//  ContentView.swift
//  assign3
//  Omar Amin
//  4/07/2021
//

import SwiftUI

extension CGPoint: CustomStringConvertible {
    public var description: String { String(format: "(%.2f,%.2f)", x, y) }
}

struct MainView: View {
    @EnvironmentObject var board: Triples
    
    var body: some View {
        TabView {
            ContentView().tabItem {
                Label("Board", systemImage: "gamecontroller")
            }
            HighScores().tabItem {
                Label("Scores", systemImage: "list.dash")
            }
            About().tabItem {
                Label("About", systemImage: "info.circle")
            }
        }
    }
}

struct About: View {
    @State var spin = false
    @Environment(\.verticalSizeClass) var verticalSizeClass: UserInterfaceSizeClass?
    private let forever = Animation.linear(duration: 1.2).repeatForever(autoreverses: false)
    
    var body: some View {
        if verticalSizeClass == .regular {
            VStack {
                Text("Omar Amin\nCMSC426\nAssign3")
                    .font(.title)
                    .fontWeight(.thin)
                    .foregroundColor(Color.black)
                    .padding()
                    .multilineTextAlignment(.center)
                VStack {
                    HStack {
                        Ellipse()
                            .frame(width: 30, height: 150)
                            .foregroundColor(self.spin ? .green : .blue)
                            .rotationEffect(Angle(degrees: -45))
                            .rotation3DEffect(self.spin ? Angle(degrees: 0): Angle(degrees: 360), axis: (x: 0.0, y: 0.0, z: 1.0))
                        Spacer()
                            .frame(width: 70)
                        Ellipse()
                            .frame(width: 30, height: 150)
                            .foregroundColor(self.spin ? .yellow : .red)
                            .rotation3DEffect(self.spin ? Angle(degrees: 0): Angle(degrees: 360), axis: (x: 0.0, y: 1.0, z: 0.0))
                        Spacer()
                            .frame(width: 70)
                        Ellipse()
                            .frame(width: 30, height: 150)
                            .foregroundColor(self.spin ? .green : .blue)
                            .rotationEffect(Angle(degrees: 45))
                            .rotation3DEffect(self.spin ? Angle(degrees: 0): Angle(degrees: 360), axis: (x: 0.0, y: 0.0, z: 1.0))
                    }
                    HStack {
                        Ellipse()
                            .frame(width: 150, height: 30)
                            .foregroundColor(self.spin ? .blue : .purple)
                            .rotation3DEffect(self.spin ? Angle(degrees: 0): Angle(degrees: 360), axis: (x: 1.0, y: 0.0, z: 0.0))
                        Spacer()
                            .frame(width: 60)
                        Ellipse()
                            .frame(width: 150, height: 30)
                            .foregroundColor(self.spin ? .blue : .purple)
                            .rotation3DEffect(self.spin ? Angle(degrees: 0): Angle(degrees: 360), axis: (x: 1.0, y: 0.0, z: 0.0))
                    }
                    HStack {
                        Ellipse()
                            .frame(width: 30, height: 150)
                            .foregroundColor(self.spin ? .green : .blue)
                            .rotationEffect(Angle(degrees: 45))
                            .rotation3DEffect(self.spin ? Angle(degrees: 0): Angle(degrees: 360), axis: (x: 0.0, y: 0.0, z: 1.0))
                        Spacer()
                            .frame(width: 70)
                        Ellipse()
                            .frame(width: 30, height: 150)
                            .foregroundColor(self.spin ? .yellow : .red)
                            .rotation3DEffect(self.spin ? Angle(degrees: 0): Angle(degrees: 360), axis: (x: 0.0, y: 1.0, z: 0.0))
                        Spacer()
                            .frame(width: 70)
                        Ellipse()
                            .frame(width: 30, height: 150)
                            .foregroundColor(self.spin ? .green : .blue)
                            .rotationEffect(Angle(degrees: -45))
                            .rotation3DEffect(self.spin ? Angle(degrees: 0): Angle(degrees: 360), axis: (x: 0.0, y: 0.0, z: 1.0))
                    }
                }
                .rotationEffect(self.spin ? .degrees(0) : .degrees(360))
            }
            .onAppear {
                withAnimation (self.forever, {
                    self.spin.toggle()
                })
            }
        }
        else {
            HStack {
                Text("Omar Amin\nCMSC426\nAssign3")
                    .font(.title)
                    .fontWeight(.thin)
                    .foregroundColor(Color.black)
                    .padding()
                    .multilineTextAlignment(.center)
                Spacer()
                    .frame(width: 50)
                VStack {
                    HStack {
                        Ellipse()
                            .frame(width: 30, height: 130)
                            .foregroundColor(self.spin ? .green : .blue)
                            .rotationEffect(Angle(degrees: -45))
                            .rotation3DEffect(self.spin ? Angle(degrees: 0): Angle(degrees: 360), axis: (x: 0.0, y: 0.0, z: 1.0))
                        Spacer()
                            .frame(width: 70)
                        Ellipse()
                            .frame(width: 30, height: 130)
                            .foregroundColor(self.spin ? .yellow : .red)
                            .rotation3DEffect(self.spin ? Angle(degrees: 0): Angle(degrees: 360), axis: (x: 0.0, y: 1.0, z: 0.0))
                        Spacer()
                            .frame(width: 70)
                        Ellipse()
                            .frame(width: 30, height: 130)
                            .foregroundColor(self.spin ? .green : .blue)
                            .rotationEffect(Angle(degrees: 45))
                            .rotation3DEffect(self.spin ? Angle(degrees: 0): Angle(degrees: 360), axis: (x: 0.0, y: 0.0, z: 1.0))
                    }
                    HStack {
                        Ellipse()
                            .frame(width: 130, height: 30)
                            .foregroundColor(self.spin ? .blue : .purple)
                            .rotation3DEffect(self.spin ? Angle(degrees: 0): Angle(degrees: 360), axis: (x: 1.0, y: 0.0, z: 0.0))
                        Spacer()
                            .frame(width: 60)
                        Ellipse()
                            .frame(width: 130, height: 30)
                            .foregroundColor(self.spin ? .blue : .purple)
                            .rotation3DEffect(self.spin ? Angle(degrees: 0): Angle(degrees: 360), axis: (x: 1.0, y: 0.0, z: 0.0))
                    }
                    HStack {
                        Ellipse()
                            .frame(width: 30, height: 130)
                            .foregroundColor(self.spin ? .green : .blue)
                            .rotationEffect(Angle(degrees: 45))
                            .rotation3DEffect(self.spin ? Angle(degrees: 0): Angle(degrees: 360), axis: (x: 0.0, y: 0.0, z: 1.0))
                        Spacer()
                            .frame(width: 70)
                        Ellipse()
                            .frame(width: 30, height: 130)
                            .foregroundColor(self.spin ? .yellow : .red)
                            .rotation3DEffect(self.spin ? Angle(degrees: 0): Angle(degrees: 360), axis: (x: 0.0, y: 1.0, z: 0.0))
                        Spacer()
                            .frame(width: 70)
                        Ellipse()
                            .frame(width: 30, height: 130)
                            .foregroundColor(self.spin ? .green : .blue)
                            .rotationEffect(Angle(degrees: -45))
                            .rotation3DEffect(self.spin ? Angle(degrees: 0): Angle(degrees: 360), axis: (x: 0.0, y: 0.0, z: 1.0))
                    }
                }
                .rotationEffect(self.spin ? .degrees(0) : .degrees(360))
            }
            .onAppear {
                withAnimation (self.forever, {
                    self.spin.toggle()
                })
            }
        }
    }
}

struct HighScores: View {
    @EnvironmentObject var board: Triples
    
    var timeFm: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm:ss"
        return formatter
    }
    
    var body: some View {
        VStack() {
            Text("High Scores")
                .font(.title)
                .fontWeight(.thin)
                .foregroundColor(Color.black)
                .padding()
            List(board.allScores.sorted(by: { $0.score > $1.score }), id: \.self) { sc in
                HStack() {
                    Text("\(board.allScores.sorted(by: { $0.score > $1.score }).firstIndex(of: sc)! + 1))")
                        .frame(width: 30)
                    Text("\(sc.score)")
                        .frame(width: 130)
                    Text("\(sc.time, style: .date) \(sc.time, formatter: timeFm)")
                }
            }
        }
        
    }
}

struct ContentView: View {
    @EnvironmentObject var board: Triples
    @State var flips: [Bool] = [Bool](repeating: false, count: 16)
    @State var selected: Bool = false
    @Environment(\.verticalSizeClass) var verticalSizeClass: UserInterfaceSizeClass?
    
    @State private var current: CGSize = .zero
    @State private var startPoint: CGPoint = .zero
    @State private var endPoint: CGPoint = .zero
    
    var drag: some Gesture {
        DragGesture(minimumDistance: 10.0, coordinateSpace: .global)
            .onEnded { v in
                if v.translation.height < 0 {
                    if (v.translation.width <= 0 && v.translation.width > -20) || (v.translation.width >= 0 && v.translation.width < 20){
                        if board.collapse(dir: .up) == true {
                            board.spawn()
                        }
                        else {
                            board.spawn()
                        }
                        board.gameOver()
                    }
                }
                else if v.translation.height > 0 {
                    if (v.translation.width <= 0 && v.translation.width > -20) || (v.translation.width >= 0 && v.translation.width < 20){
                        if board.collapse(dir: .down) == true {
                            board.spawn()
                        }
                        else {
                            board.spawn()
                        }
                        board.gameOver()
                    }
                }
                if v.translation.width >= 20 {
                    if (v.translation.height <= 0 && v.translation.height > -20) || (v.translation.height >= 0 && v.translation.height < 20){
                        if board.collapse(dir: .right) == true {
                            board.spawn()
                        }
                        else {
                            board.spawn()
                        }
                        board.gameOver()
                    }
                }
                else if v.translation.width <= -20 {
                    if (v.translation.height <= 0 && v.translation.height > -20) || (v.translation.height >= 0 && v.translation.height < 20){
                        if board.collapse(dir: .left) == true {
                            board.spawn()
                        }
                        else {
                            board.spawn()
                        }
                        board.gameOver()
                    }
                }
            }
    }
    
    var body: some View {
        ZStack {
            if verticalSizeClass == .regular {
                VStack() {
                    Score(board.score)
                    VStack() {
                        HStack() {
                            TL(board.board[0][0], flipped: self.$flips[0])
                                .rotation3DEffect(self.flips[0] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                    .animation(.default)
                            TM1(board.board[0][1], flipped: self.$flips[1])
                                .rotation3DEffect(self.flips[1] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                    .animation(.default)
                            TM2(board.board[0][2], flipped: self.$flips[2])
                                .rotation3DEffect(self.flips[2] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                    .animation(.default)
                            TR(board.board[0][3], flipped: self.$flips[3])
                                .rotation3DEffect(self.flips[3] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                    .animation(.default)
                        }
                        HStack() {
                            M1L(board.board[1][0], flipped: self.$flips[4])
                                .rotation3DEffect(self.flips[4] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                    .animation(.default)
                            M1M1(board.board[1][1], flipped: self.$flips[5])
                                .rotation3DEffect(self.flips[5] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                    .animation(.default)
                            M1M2(board.board[1][2], flipped: self.$flips[6])
                                .rotation3DEffect(self.flips[6] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                    .animation(.default)
                            M1R(board.board[1][3], flipped: self.$flips[7])
                                .rotation3DEffect(self.flips[7] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                    .animation(.default)
                        }
                        HStack() {
                            M2L(board.board[2][0], flipped: self.$flips[8])
                                .rotation3DEffect(self.flips[8] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                    .animation(.default)
                            M2M1(board.board[2][1], flipped: self.$flips[9])
                                .rotation3DEffect(self.flips[9] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                    .animation(.default)
                            M2M2(board.board[2][2], flipped: self.$flips[10])
                                .rotation3DEffect(self.flips[10] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                    .animation(.default)
                            M2R(board.board[2][3], flipped: self.$flips[11])
                                .rotation3DEffect(self.flips[11] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                    .animation(.default)
                        }
                        HStack() {
                            BL(board.board[3][0], flipped: self.$flips[12])
                                .rotation3DEffect(self.flips[12] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                    .animation(.default)
                            BM1(board.board[3][1], flipped: self.$flips[13])
                                .rotation3DEffect(self.flips[13] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                    .animation(.default)
                            BM2(board.board[3][2], flipped: self.$flips[14])
                                .rotation3DEffect(self.flips[14] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                    .animation(.default)
                            BR(board.board[3][3], flipped: self.$flips[15])
                                .rotation3DEffect(self.flips[15] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                    .animation(.default)
                        }
                    }
                    .padding()
                    .overlay(
                        RoundedRectangle(cornerRadius: 15)
                            .stroke(lineWidth: 2)
                            .foregroundColor(.black)
                    )
                    .gesture(drag)
                    UpButton(board)
                    HStack() {
                        LeftButton(board)
                        RightButton(board)
                    }
                    VStack() {
                        DownButton(board)
                        GameButton(board, selected: self.$selected)
                    }
                }
                .blur(radius: board.isDone ? 5 : 0)
                if board.isDone == true {
                    GameOver(board, selected: self.$selected)
                }
            }
            else {
                HStack() {
                    VStack {
                        Score(board.score)
                            .offset(y: 35)
                        VStack() {
                            HStack() {
                                TL(board.board[0][0], flipped: self.$flips[0])
                                    .rotation3DEffect(self.flips[0] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                        .animation(.default)
                                TM1(board.board[0][1], flipped: self.$flips[1])
                                    .rotation3DEffect(self.flips[1] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                        .animation(.default)
                                TM2(board.board[0][2], flipped: self.$flips[2])
                                    .rotation3DEffect(self.flips[2] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                        .animation(.default)
                                TR(board.board[0][3], flipped: self.$flips[3])
                                    .rotation3DEffect(self.flips[3] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                        .animation(.default)
                            }
                            HStack() {
                                M1L(board.board[1][0], flipped: self.$flips[4])
                                    .rotation3DEffect(self.flips[4] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                        .animation(.default)
                                M1M1(board.board[1][1], flipped: self.$flips[5])
                                    .rotation3DEffect(self.flips[5] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                        .animation(.default)
                                M1M2(board.board[1][2], flipped: self.$flips[6])
                                    .rotation3DEffect(self.flips[6] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                        .animation(.default)
                                M1R(board.board[1][3], flipped: self.$flips[7])
                                    .rotation3DEffect(self.flips[7] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                        .animation(.default)
                            }
                            HStack() {
                                M2L(board.board[2][0], flipped: self.$flips[8])
                                    .rotation3DEffect(self.flips[8] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                        .animation(.default)
                                M2M1(board.board[2][1], flipped: self.$flips[9])
                                    .rotation3DEffect(self.flips[9] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                        .animation(.default)
                                M2M2(board.board[2][2], flipped: self.$flips[10])
                                    .rotation3DEffect(self.flips[10] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                        .animation(.default)
                                M2R(board.board[2][3], flipped: self.$flips[11])
                                    .rotation3DEffect(self.flips[11] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                        .animation(.default)
                            }
                            HStack() {
                                BL(board.board[3][0], flipped: self.$flips[12])
                                    .rotation3DEffect(self.flips[12] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                        .animation(.default)
                                BM1(board.board[3][1], flipped: self.$flips[13])
                                    .rotation3DEffect(self.flips[13] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                        .animation(.default)
                                BM2(board.board[3][2], flipped: self.$flips[14])
                                    .rotation3DEffect(self.flips[14] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                        .animation(.default)
                                BR(board.board[3][3], flipped: self.$flips[15])
                                    .rotation3DEffect(self.flips[15] ? Angle(degrees: 360): Angle(degrees: 0), axis: (x: 0.0, y: 1.0, z: 0.0))
                                        .animation(.default)
                            }
                        }
                        .padding()
                        .overlay(
                            RoundedRectangle(cornerRadius: 15)
                                .stroke(lineWidth: 2)
                                .foregroundColor(.black)
                        )
                        .scaleEffect(x:0.8, y: 0.8)
                        .gesture(drag)
                    }
                    VStack {
                        UpButton(board)
                        HStack() {
                            LeftButton(board)
                            RightButton(board)
                        }
                        DownButton(board)
                        GameButton(board, selected: self.$selected)
                    }
                }
                .blur(radius: board.isDone ? 5 : 0)
                if board.isDone == true {
                    GameOver(board, selected: self.$selected)
                }
            }
        }
    }
}

struct GameOver: View {
    @Binding var selected: Bool
    private var board: Triples

    init(_ board: Triples, selected: Binding<Bool>) {
        self.board = board
        self._selected = selected
    }
    
    var body: some View {
        VStack() {
            Text("Game Over!")
                .font(.title)
                .fontWeight(.thin)
                .foregroundColor(Color.white)
                .padding()
            Text("Score: " + String(board.allScores.last?.score ?? 0))
                .font(.title)
                .fontWeight(.thin)
                .foregroundColor(Color.white)
                .padding()
            Button(action: f) {
                Text("Play Again")
                    .font(.largeTitle)
                    .fontWeight(.light)
                    .padding(6.0)
            }
            .frame(width: 200, height: 50)
            .background(Color.green)
            .cornerRadius(30)
            .accentColor(/*@START_MENU_TOKEN@*/.white/*@END_MENU_TOKEN@*/)
            .padding()
            .shadow(color: .gray, radius: 1, x: 0, y: 2)
        }
        .frame(width: 315, height: 250)
        .background(Color.black)
        .cornerRadius(30)
    }
    
    func f() {
        board.newgame(rand: selected)
        for _ in 0...3 {
            board.spawn()
        }
        board.isDone = false
    }
}

struct Score: View {
    private var score: Int
    
    init(_ score: Int) {
        self.score = score
    }
    
    var body: some View {
        Text("Score: " + String(score))
            .font(.title)
            .fontWeight(.thin)
    }
}

struct TL: View {
    private var val: Int = 0
    @Binding var flipped: Bool
    @State var shown: Bool = false
    @State var prev: Int = 0
    
    init(_ value: Int, flipped: Binding<Bool>) {
        val = value
        self._flipped = flipped
        prev = value
    }
    
    var body: some View {
        switch val {
        case 0:
            Text("")
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.black)
                .opacity(0.15)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 0
                }
        case 1:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.red)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 1
                }
        case 2:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.orange)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 2
                }
        case 3:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.yellow)
                .cornerRadius(15)
                .onAppear {
                    if prev == 1 || prev == 2 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = 3
                }
        default:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.purple)
                .cornerRadius(15)
                .onAppear {
                    if prev % 3 == 0 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = val
                }
                .onChange(of: val) { value in
                    shown = false
                    self.f()
                    shown = false
                    prev = val
                }
        }
    }
    
    func f() {
        if shown == false {
            flipped.toggle()
        }
        else {
            shown = false
        }
    }
}

struct TM1: View {
    private var val: Int = 0
    @Binding var flipped: Bool
    @State var shown: Bool = false
    @State var prev: Int = 0
    
    init(_ value: Int, flipped: Binding<Bool>) {
        val = value
        self._flipped = flipped
        prev = value
    }
    
    var body: some View {
        switch val {
        case 0:
            Text("")
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.black)
                .opacity(0.15)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 0
                }
        case 1:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.red)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 1
                }
        case 2:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.orange)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 2
                }
        case 3:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.yellow)
                .cornerRadius(15)
                .onAppear {
                    if prev == 1 || prev == 2 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = 3
                }
        default:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.purple)
                .cornerRadius(15)
                .onAppear {
                    if prev % 3 == 0 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = val
                }
                .onChange(of: val) { value in
                    shown = false
                    self.f()
                    shown = false
                    prev = val
                }
        }
    }
    
    func f() {
        if shown == false {
            flipped.toggle()
        }
        else {
            shown = false
        }
    }
}

struct TM2: View {
    private var val: Int = 0
    @Binding var flipped: Bool
    @State var shown: Bool = false
    @State var prev: Int = 0
    
    init(_ value: Int, flipped: Binding<Bool>) {
        val = value
        self._flipped = flipped
        prev = value
    }
    
    var body: some View {
        switch val {
        case 0:
            Text("")
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.black)
                .opacity(0.15)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 0
                }
        case 1:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.red)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 1
                }
        case 2:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.orange)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 2
                }
        case 3:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.yellow)
                .cornerRadius(15)
                .onAppear {
                    if prev == 1 || prev == 2 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = 3
                }
        default:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.purple)
                .cornerRadius(15)
                .onAppear {
                    if prev % 3 == 0 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = val
                }
                .onChange(of: val) { value in
                    shown = false
                    self.f()
                    shown = false
                    prev = val
                }
        }
    }
    
    func f() {
        if shown == false {
            flipped.toggle()
        }
        else {
            shown = false
        }
    }
}

struct TR: View {
    private var val: Int = 0
    @Binding var flipped: Bool
    @State var shown: Bool = false
    @State var prev: Int = 0
    
    init(_ value: Int, flipped: Binding<Bool>) {
        val = value
        self._flipped = flipped
        prev = value
    }
    
    var body: some View {
        switch val {
        case 0:
            Text("")
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.black)
                .opacity(0.15)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 0
                }
        case 1:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.red)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 1
                }
        case 2:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.orange)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 2
                }
        case 3:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.yellow)
                .cornerRadius(15)
                .onAppear {
                    if prev == 1 || prev == 2 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = 3
                }
        default:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.purple)
                .cornerRadius(15)
                .onAppear {
                    if prev % 3 == 0 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = val
                }
                .onChange(of: val) { value in
                    shown = false
                    self.f()
                    shown = false
                    prev = val
                }
        }
    }
    
    func f() {
        if shown == false {
            flipped.toggle()
        }
        else {
            shown = false
        }
    }
}

struct M1L: View {
    private var val: Int = 0
    @Binding var flipped: Bool
    @State var shown: Bool = false
    @State var prev: Int = 0
    
    init(_ value: Int, flipped: Binding<Bool>) {
        val = value
        self._flipped = flipped
        prev = value
    }
    
    var body: some View {
        switch val {
        case 0:
            Text("")
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.black)
                .opacity(0.15)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 0
                }
        case 1:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.red)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 1
                }
        case 2:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.orange)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 2
                }
        case 3:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.yellow)
                .cornerRadius(15)
                .onAppear {
                    if prev == 1 || prev == 2 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = 3
                }
        default:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.purple)
                .cornerRadius(15)
                .onAppear {
                    if prev % 3 == 0 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = val
                }
                .onChange(of: val) { value in
                    shown = false
                    self.f()
                    shown = false
                    prev = val
                }
        }
    }
    
    func f() {
        if shown == false {
            flipped.toggle()
        }
        else {
            shown = false
        }
    }
}

struct M1M1: View {
    private var val: Int = 0
    @Binding var flipped: Bool
    @State var shown: Bool = false
    @State var prev: Int = 0
    
    init(_ value: Int, flipped: Binding<Bool>) {
        val = value
        self._flipped = flipped
        prev = value
    }
    
    var body: some View {
        switch val {
        case 0:
            Text("")
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.black)
                .opacity(0.15)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 0
                }
        case 1:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.red)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 1
                }
        case 2:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.orange)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 2
                }
        case 3:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.yellow)
                .cornerRadius(15)
                .onAppear {
                    if prev == 1 || prev == 2 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = 3
                }
        default:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.purple)
                .cornerRadius(15)
                .onAppear {
                    if prev % 3 == 0 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = val
                }
                .onChange(of: val) { value in
                    shown = false
                    self.f()
                    shown = false
                    prev = val
                }
        }
    }
    
    func f() {
        if shown == false {
            flipped.toggle()
        }
        else {
            shown = false
        }
    }
}

struct M1M2: View {
    private var val: Int = 0
    @Binding var flipped: Bool
    @State var shown: Bool = false
    @State var prev: Int = 0
    
    init(_ value: Int, flipped: Binding<Bool>) {
        val = value
        self._flipped = flipped
        prev = value
    }
    
    var body: some View {
        switch val {
        case 0:
            Text("")
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.black)
                .opacity(0.15)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 0
                }
        case 1:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.red)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 1
                }
        case 2:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.orange)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 2
                }
        case 3:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.yellow)
                .cornerRadius(15)
                .onAppear {
                    if prev == 1 || prev == 2 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = 3
                }
        default:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.purple)
                .cornerRadius(15)
                .onAppear {
                    if prev % 3 == 0 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = val
                }
                .onChange(of: val) { value in
                    shown = false
                    self.f()
                    shown = false
                    prev = val
                }
        }
    }
    
    func f() {
        if shown == false {
            flipped.toggle()
        }
        else {
            shown = false
        }
    }
}

struct M1R: View {
    private var val: Int = 0
    @Binding var flipped: Bool
    @State var shown: Bool = false
    @State var prev: Int = 0
    
    init(_ value: Int, flipped: Binding<Bool>) {
        val = value
        self._flipped = flipped
        prev = value
    }
    
    var body: some View {
        switch val {
        case 0:
            Text("")
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.black)
                .opacity(0.15)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 0
                }
        case 1:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.red)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 1
                }
        case 2:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.orange)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 2
                }
        case 3:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.yellow)
                .cornerRadius(15)
                .onAppear {
                    if prev == 1 || prev == 2 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = 3
                }
        default:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.purple)
                .cornerRadius(15)
                .onAppear {
                    if prev % 3 == 0 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = val
                }
                .onChange(of: val) { value in
                    shown = false
                    self.f()
                    shown = false
                    prev = val
                }
        }
    }
    
    func f() {
        if shown == false {
            flipped.toggle()
        }
        else {
            shown = false
        }
    }
}

struct M2L: View {
    private var val: Int = 0
    @Binding var flipped: Bool
    @State var shown: Bool = false
    @State var prev: Int = 0
    
    init(_ value: Int, flipped: Binding<Bool>) {
        val = value
        self._flipped = flipped
        prev = value
    }
    
    var body: some View {
        switch val {
        case 0:
            Text("")
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.black)
                .opacity(0.15)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 0
                }
        case 1:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.red)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 1
                }
        case 2:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.orange)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 2
                }
        case 3:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.yellow)
                .cornerRadius(15)
                .onAppear {
                    if prev == 1 || prev == 2 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = 3
                }
        default:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.purple)
                .cornerRadius(15)
                .onAppear {
                    if prev % 3 == 0 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = val
                }
                .onChange(of: val) { value in
                    shown = false
                    self.f()
                    shown = false
                    prev = val
                }
        }
    }
    
    func f() {
        if shown == false {
            flipped.toggle()
        }
        else {
            shown = false
        }
    }
}

struct M2M1: View {
    private var val: Int = 0
    @Binding var flipped: Bool
    @State var shown: Bool = false
    @State var prev: Int = 0
    
    init(_ value: Int, flipped: Binding<Bool>) {
        val = value
        self._flipped = flipped
        prev = value
    }
    
    var body: some View {
        switch val {
        case 0:
            Text("")
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.black)
                .opacity(0.15)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 0
                }
        case 1:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.red)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 1
                }
        case 2:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.orange)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 2
                }
        case 3:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.yellow)
                .cornerRadius(15)
                .onAppear {
                    if prev == 1 || prev == 2 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = 3
                }
        default:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.purple)
                .cornerRadius(15)
                .onAppear {
                    if prev % 3 == 0 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = val
                }
                .onChange(of: val) { value in
                    shown = false
                    self.f()
                    shown = false
                    prev = val
                }
        }
    }
    
    func f() {
        if shown == false {
            flipped.toggle()
        }
        else {
            shown = false
        }
    }
}

struct M2M2: View {
    private var val: Int = 0
    @Binding var flipped: Bool
    @State var shown: Bool = false
    @State var prev: Int = 0
    
    init(_ value: Int, flipped: Binding<Bool>) {
        val = value
        self._flipped = flipped
        prev = value
    }
    
    var body: some View {
        switch val {
        case 0:
            Text("")
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.black)
                .opacity(0.15)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 0
                }
        case 1:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.red)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 1
                }
        case 2:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.orange)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 2
                }
        case 3:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.yellow)
                .cornerRadius(15)
                .onAppear {
                    if prev == 1 || prev == 2 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = 3
                }
        default:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.purple)
                .cornerRadius(15)
                .onAppear {
                    if prev % 3 == 0 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = val
                }
                .onChange(of: val) { value in
                    shown = false
                    self.f()
                    shown = false
                    prev = val
                }
        }
    }
    
    func f() {
        if shown == false {
            flipped.toggle()
        }
        else {
            shown = false
        }
    }
}

struct M2R: View {
    private var val: Int = 0
    @Binding var flipped: Bool
    @State var shown: Bool = false
    @State var prev: Int = 0
    
    init(_ value: Int, flipped: Binding<Bool>) {
        val = value
        self._flipped = flipped
        prev = value
    }
    
    var body: some View {
        switch val {
        case 0:
            Text("")
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.black)
                .opacity(0.15)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 0
                }
        case 1:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.red)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 1
                }
        case 2:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.orange)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 2
                }
        case 3:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.yellow)
                .cornerRadius(15)
                .onAppear {
                    if prev == 1 || prev == 2 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = 3
                }
        default:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.purple)
                .cornerRadius(15)
                .onAppear {
                    if prev % 3 == 0 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = val
                }
                .onChange(of: val) { value in
                    shown = false
                    self.f()
                    shown = false
                    prev = val
                }
        }
    }
    
    func f() {
        if shown == false {
            flipped.toggle()
        }
        else {
            shown = false
        }
    }
}

struct BL: View {
    private var val: Int = 0
    @Binding var flipped: Bool
    @State var shown: Bool = false
    @State var prev: Int = 0
    
    init(_ value: Int, flipped: Binding<Bool>) {
        val = value
        self._flipped = flipped
        prev = value
    }
    
    var body: some View {
        switch val {
        case 0:
            Text("")
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.black)
                .opacity(0.15)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 0
                }
        case 1:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.red)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 1
                }
        case 2:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.orange)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 2
                }
        case 3:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.yellow)
                .cornerRadius(15)
                .onAppear {
                    if prev == 1 || prev == 2 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = 3
                }
        default:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.purple)
                .cornerRadius(15)
                .onAppear {
                    if prev % 3 == 0 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = val
                }
                .onChange(of: val) { value in
                    shown = false
                    self.f()
                    shown = false
                    prev = val
                }
        }
    }
    
    func f() {
        if shown == false {
            flipped.toggle()
        }
        else {
            shown = false
        }
    }
}

struct BM1: View {
    private var val: Int = 0
    @Binding var flipped: Bool
    @State var shown: Bool = false
    @State var prev: Int = 0
    
    init(_ value: Int, flipped: Binding<Bool>) {
        val = value
        self._flipped = flipped
        prev = value
    }
    
    var body: some View {
        switch val {
        case 0:
            Text("")
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.black)
                .opacity(0.15)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 0
                }
        case 1:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.red)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 1
                }
        case 2:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.orange)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 2
                }
        case 3:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.yellow)
                .cornerRadius(15)
                .onAppear {
                    if prev == 1 || prev == 2 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = 3
                }
        default:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.purple)
                .cornerRadius(15)
                .onAppear {
                    if prev % 3 == 0 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = val
                }
                .onChange(of: val) { value in
                    shown = false
                    self.f()
                    shown = false
                    prev = val
                }
        }
    }
    
    func f() {
        if shown == false {
            flipped.toggle()
        }
        else {
            shown = false
        }
    }
}

struct BM2: View {
    private var val: Int = 0
    @Binding var flipped: Bool
    @State var shown: Bool = false
    @State var prev: Int = 0
    
    init(_ value: Int, flipped: Binding<Bool>) {
        val = value
        self._flipped = flipped
        prev = value
    }
    
    var body: some View {
        switch val {
        case 0:
            Text("")
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.black)
                .opacity(0.15)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 0
                }
        case 1:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.red)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 1
                }
        case 2:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.orange)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 2
                }
        case 3:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.yellow)
                .cornerRadius(15)
                .onAppear {
                    if prev == 1 || prev == 2 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = 3
                }
        default:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.purple)
                .cornerRadius(15)
                .onAppear {
                    if prev % 3 == 0 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = val
                }
                .onChange(of: val) { value in
                    shown = false
                    self.f()
                    shown = false
                    prev = val
                }
        }
    }
    
    func f() {
        if shown == false {
            flipped.toggle()
        }
        else {
            shown = false
        }
    }
}

struct BR: View {
    private var val: Int = 0
    @Binding var flipped: Bool
    @State var shown: Bool = false
    @State var prev: Int = 0
    
    init(_ value: Int, flipped: Binding<Bool>) {
        val = value
        self._flipped = flipped
        prev = value
    }
    
    var body: some View {
        switch val {
        case 0:
            Text("")
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.black)
                .opacity(0.15)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 0
                }
        case 1:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.red)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 1
                }
        case 2:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.orange)
                .cornerRadius(15)
                .onAppear {
                    shown = true
                    prev = 2
                }
        case 3:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.yellow)
                .cornerRadius(15)
                .onAppear {
                    if prev == 1 || prev == 2 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = 3
                }
        default:
            Text(String(val))
                .font(.title)
                .fontWeight(.thin)
                .padding()
                .frame(width: 75, height: 75)
                .background(Color.purple)
                .cornerRadius(15)
                .onAppear {
                    if prev % 3 == 0 {
                        shown = false
                    }
                    self.f()
                    shown = true
                    prev = val
                }
                .onChange(of: val) { value in
                    shown = false
                    self.f()
                    shown = false
                    prev = val
                }
        }
    }
    
    func f() {
        if shown == false {
            flipped.toggle()
        }
        else {
            shown = false
        }
    }
}

struct UpButton: View {
    @State private var buttonText = "↑"
    private var board: Triples
    
    init(_ board: Triples) {
        self.board = board
    }
    
    var body: some View {
        Button(action: f) {
            Text(buttonText)
                .font(.largeTitle)
                .fontWeight(.light)
                .padding(6.0)
        }
        .frame(width: 100.0, height: 53)
        .background(Color.pink)
        .cornerRadius(15)
        .accentColor(/*@START_MENU_TOKEN@*/.white/*@END_MENU_TOKEN@*/)
        .shadow(color: .gray, radius: 1, x: 0, y: 2)
        }
    
    func f() {
        if board.collapse(dir: .up) == true {
            board.spawn()
        }
        else {
            board.spawn()
        }
        board.gameOver()
    }
}

struct LeftButton: View {
    @State private var buttonText = "←"
    private var board: Triples
    
    init(_ board: Triples) {
        self.board = board
    }
    
    var body: some View {
        Button(action: f) {
            Text(buttonText)
                .font(.largeTitle)
                .fontWeight(.light)
                .padding(6.0)
        }
        .frame(width: 100.0, height: 53)
        .background(Color.pink)
        .cornerRadius(15)
        .accentColor(/*@START_MENU_TOKEN@*/.white/*@END_MENU_TOKEN@*/)
        .shadow(color: .gray, radius: 1, x: 0, y: 2)
        }
    
    func f() {
        if board.collapse(dir: .left) == true {
            board.spawn()
        }
        else {
            board.spawn()
        }
        board.gameOver()
    }
}

struct RightButton: View {
    @State private var buttonText = "→"
    private var board: Triples
    
    init(_ board: Triples) {
        self.board = board
    }
    
    var body: some View {
        Button(action: f) {
            Text(buttonText)
                .font(.largeTitle)
                .fontWeight(.light)
                .padding(6.0)
        }
        .frame(width: 100.0, height: 53)
        .background(Color.pink)
        .cornerRadius(15)
        .accentColor(/*@START_MENU_TOKEN@*/.white/*@END_MENU_TOKEN@*/)
        .shadow(color: .gray, radius: 1, x: 0, y: 2)
        }
    
    func f() {
        if board.collapse(dir: .right) == true {
            board.spawn()
        }
        else {
            board.spawn()
        }
        board.gameOver()
    }
}

struct DownButton: View {
    @State private var buttonText = "↓"
    private var board: Triples
    
    init(_ board: Triples) {
        self.board = board
    }
    
    var body: some View {
        Button(action: f) {
            Text(buttonText)
                .font(.largeTitle)
                .fontWeight(.light)
                .padding(6.0)
        }
        .frame(width: 100.0, height: 53)
        .background(Color.pink)
        .cornerRadius(15)
        .accentColor(/*@START_MENU_TOKEN@*/.white/*@END_MENU_TOKEN@*/)
        .shadow(color: .gray, radius: 1, x: 0, y: 2)
        }
    
    func f() {
        if board.collapse(dir: .down) == true {
            board.spawn()
        }
        else {
            board.spawn()
        }
        board.gameOver()
    }
}

struct GameButton: View {
    @State private var buttonText = "New Game"
    @Binding var selected: Bool
    private var board: Triples
    
    init(_ board: Triples, selected: Binding<Bool>) {
        self.board = board
        self._selected = selected
    }
    
    var body: some View {
        VStack {
            Button(action: f) {
                Text(buttonText)
                    .font(.largeTitle)
                    .fontWeight(.light)
                    .padding(6.0)
            }
            .frame(width: 200, height: 50)
            .background(Color.green)
            .cornerRadius(30)
            .accentColor(/*@START_MENU_TOKEN@*/.white/*@END_MENU_TOKEN@*/)
            .shadow(color: .gray, radius: 1, x: 0, y: 2)
            
            HStack() {
                Text("Random")
                    .font(.title2)
                    .fontWeight(.light)
                    .padding()
                Toggle("Label", isOn: $selected)
                    .padding()
                    .labelsHidden()
                Text("Determ")
                    .font(.title2)
                    .fontWeight(.light)
                    .padding()
            }
            .overlay(
                RoundedRectangle(cornerRadius: 15)
                    .stroke(lineWidth: 2)
                    .foregroundColor(selected ? .green : .gray)
                    .frame(height: 50)
            )
        }
    }
    
    func f() {
        board.allScores.append(ScoreType(score: board.score, time: Date()))
        board.newgame(rand: selected)
        for _ in 0...3 {
            board.spawn()
        }
        board.isDone = true
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        MainView().environmentObject(Triples())
    }
}
